<?php /* Smarty version 2.6.14, created on 2014-04-07 23:49:57
         compiled from gallery/gallery.tpl */ ?>
<?php echo $this->_tpl_vars['modules']['head']; ?>


<div id="blockGallery">
    <div id="inner-block" align="center">

    <?php if (! ( $this->_tpl_vars['final'] )): ?>
        <?php echo $this->_tpl_vars['modules']['animal']; ?>


        <?php if ($this->_tpl_vars['numero'] != 0): ?>
            <a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/gallery/<?php echo $this->_tpl_vars['anterior']; ?>
">
                Anterior
            </a>
        <?php endif; ?>
            <a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/gallery/<?php echo $this->_tpl_vars['seguent']; ?>
">
                Següent
            </a>
    <?php else: ?>
        <br><br>
        <p>Has arribat al final de la galeria!</p>
        <br><br>
        <a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/gallery">
            Tornar
        </a>
    <?php endif; ?>
        <br><br>

    </div>
</div>

<div class="clear"></div>

<?php echo $this->_tpl_vars['modules']['footer']; ?>