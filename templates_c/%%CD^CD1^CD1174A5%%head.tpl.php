<?php /* Smarty version 2.6.14, created on 2014-04-08 01:13:48
         compiled from shared/head.tpl */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="title" content="" />
	<meta name="robots" content="all" />
	<meta name="expires" content="never" />
	<meta name="distribution" content="world" />
	<title>The Zoo Gallery</title>
	<link rel="stylesheet" href="<?php echo $this->_tpl_vars['url']['global']; ?>
/css/style.css">
</head>
<body id="home">

    <div id="menuWrapper">
        <div id='cssmenu'>
            <ul>
            <?php if ($this->_tpl_vars['link'] == 'home' || $this->_tpl_vars['link'] == 'default'): ?>
                <li class='active'><a href='<?php echo $this->_tpl_vars['url']['global']; ?>
/home'><span>Home</span></a></li>
            <?php else: ?>
                <li><a href='<?php echo $this->_tpl_vars['url']['global']; ?>
/home'><span>Home</span></a></li>
            <?php endif; ?>
            <?php if ($this->_tpl_vars['link'] == 'gallery'): ?>
                <li class='active'><a href='<?php echo $this->_tpl_vars['url']['global']; ?>
/gallery'><span>Gallery</span></a></li>
            <?php else: ?>
                <li><a href='<?php echo $this->_tpl_vars['url']['global']; ?>
/gallery '><span>Gallery</span></a></li>
            <?php endif; ?>
            <?php if ($this->_tpl_vars['link'] == 'modify'): ?>
            <li class='active'><a href='<?php echo $this->_tpl_vars['url']['global']; ?>
/modify'><span>Modify</span></a></li>
            <?php else: ?>
            <li><a href='<?php echo $this->_tpl_vars['url']['global']; ?>
/modify '><span>Modify</span></a></li>
            <?php endif; ?>
            <?php if ($this->_tpl_vars['link'] == 'upload'): ?>
            <li class='active'><a href='<?php echo $this->_tpl_vars['url']['global']; ?>
/upload'><span>Upload</span></a></li>
            <?php else: ?>
            <li><a href='<?php echo $this->_tpl_vars['url']['global']; ?>
/upload'><span>Upload</span></a></li>
            <?php endif; ?>
            <?php if ($this->_tpl_vars['link'] == 'about'): ?>
                <li class='active'><a href='<?php echo $this->_tpl_vars['url']['global']; ?>
/about'><span>About</span></a></li>
            <?php else: ?>
                <li><a href='<?php echo $this->_tpl_vars['url']['global']; ?>
/about'><span>About</span></a></li>
            <?php endif; ?>
            </ul>
        </div>
    </div>
    <div id="wrapper">