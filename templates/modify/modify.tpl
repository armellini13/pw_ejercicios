{$modules.head}
<div class="block">
    <div id="inner-block" align="center">
    {   if $modify}
         {$modules.change}
    {   else}
         {   if $delete}
                <p>Imatge esborrada correctament!</p>
         {   /if}

            {$modules.monkey}

            {$modules.platypus}

            {$modules.marmot}
    {   /if}

    </div>
</div>

<div class="clear"></div>

{$modules.footer}