<h2 class="title">Change Animal</h2>
{   if $allOk}
    <p>Gràcies per la teva participació!<p>
    <br><br>
    <a href= {$url.global}/modify>Tornar</a>
    <br><br>
{   else}
    <form class="uploadForm" id="uploadImatge" action="" method="POST">
        Nou nom de la imatge: <input type="text" id="nomImatge" name="nomImatge">
        <br>
        Nova URL de la imatge: <input type="text" id="urlImatge" name="urlImatge">
        <br>
        <input type="submit" value="Upload">
    </form>
{   /if}
