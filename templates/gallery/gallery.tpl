{$modules.head}

<div id="blockGallery">
    <div id="inner-block" align="center">

    {   if !($final)}
        {$modules.animal}

        {   if $numero != 0}
            <a href="{$url.global}/gallery/{$anterior}">
                Anterior
            </a>
        {   /if}
            <a href="{$url.global}/gallery/{$seguent}">
                Següent
            </a>
    {   else}
        <br><br>
        <p>Has arribat al final de la galeria!</p>
        <br><br>
        <a href="{$url.global}/gallery">
            Tornar
        </a>
    {   /if}
        <br><br>

    </div>
</div>

<div class="clear"></div>

{$modules.footer}