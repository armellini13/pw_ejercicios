<?php

include_once(PATH_CONTROLLERS . 'animal/animal.ctrl.php');

class AnimalMonkeyController extends AnimalAnimalController
{
    protected $view = 'animal/monkey.tpl';

    protected $animal = 'monkey';
    protected $animalName = 'monkeyName';
    protected $finalAnimal = 'finalMonkey';
    protected $animalURL_0 = 'monkeyURL_0';
    protected $animalURL_1 = 'monkeyURL_1';
    protected $animalURL_2 = 'monkeyURL_2';

    /*
        public function build(){
            parent::build();
        }
    */
    public function loadModules() {

    }

}


?>