<?php
/**
 * Home Controller: Controller d'exemple.
 *
 * Coses a tenir en compte:
 * 	-> El controller fa un "extends Controller"
 * 	-> El controller necessitar� sempre un m�tode "public function build(){...}"
 */
class ModifyModifyController extends Controller
{
    protected $view = 'modify/modify.tpl';

    /**
     * Aquest m�tode sempre s'executa i caldr� implementar-lo sempre.
     */
    public function build()
    {
        $error = false;
        $delete = false;
        $modify = false;

        // Agafem els paràmetres
        $info = $this->getParams();

        if(isset($info["url_arguments"])){

            if((sizeof($info["url_arguments"])== 3 && ($info["url_arguments"][0]=="delete" || $info["url_arguments"][0]=="change"))
                && ($info["url_arguments"][1]=="monkey" || $info["url_arguments"][1]=="marmot"
                    || $info["url_arguments"][1]=="platypus")) {

                $modifyModel = $this->getClass('GalleryGalleryModel');
                $animal = $modifyModel->getMeAnimalByID($info["url_arguments"][2],$info["url_arguments"][1]);
                if(!empty($animal)) {

                    if($info["url_arguments"][0]=="delete") {
                        $modifyModel->deleteAnimal($info["url_arguments"][2],$info["url_arguments"][1]);
                        $delete = true;
                    }
                    else {
                        $modify = true;
                    }
                }
                else
                    $error = true;

            }
            else $error = true;

            if((sizeof($info["url_arguments"]) == 1 && $info["url_arguments"][0] == ""))
                $error = false;

        }

        // Si hi ha error
        if($error)
            $this->setLayout('error/error404.tpl');

        else {

            $this->assign('delete',$delete);
            $this->assign('modify',$modify);
            $this->setLayout($this->view);

        }

    }


    /**
     * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
     * The sintax is the following:
     * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
     *
     * @return array
     */
    public function loadModules() {
        $modules['head']	= 'SharedHeadController';
        $modules['footer']	= 'SharedFooterController';
        $modules['marmot']  = 'ModifyMarmotController';
        $modules['monkey']  = 'ModifyMonkeyController';
        $modules['platypus']= 'ModifyPlatypusController';
        $modules['change']  = 'ModifyChangeController';
        return $modules;
    }
}