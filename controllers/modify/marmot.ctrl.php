<?php

class ModifyMarmotController extends Controller
{
    protected $view = 'modify/marmot.tpl';

    public function build( )
    {
        $marmotModel = $this->getClass('GalleryGalleryModel');
        $marmot = $marmotModel->getMeAnimal('marmot');

        $this->assign('marmot',$marmot);

        $this->setLayout($this->view);


    }

}


?>