<?php

class ModifyChangeController extends Controller
{
    protected $view = 'modify/change.tpl';

    public function build( )
    {
        $this->updateForm();
        $this->setLayout($this->view);
    }

    protected function updateForm()
    {

        // Per capturar una variable enviada mitjan�ant un formulari HTML pels m�todes GET/POST, has de fer servir
        // la class Filter de la seg�ent manera.
        $nom = Filter::getString('nomImatge');
        $url = Filter::getURL('urlImatge');

        // Si $nom �s un String, entrar� a l'if. Altrament, $nom ser� fals.
        if($nom && $url) {
            $this->assign('allOk',true);
            $info = $this->getParams();
            $uploadModel = $this->getClass('GalleryGalleryModel');
            $uploadModel->changeAnimal($info["url_arguments"][1],$info["url_arguments"][2],$nom, $url);

        }
    }
}

?>