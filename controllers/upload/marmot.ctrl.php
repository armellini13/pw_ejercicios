<?php

class UploadMarmotController extends Controller
{
    public function build( )
    {
        $this->updateForm();
        $this->setLayout( 'upload/marmot.tpl' );
    }

    protected function updateForm()
    {
        // Per capturar una variable enviada mitjan�ant un formulari HTML pels m�todes GET/POST, has de fer servir
        // la class Filter de la seg�ent manera.
        $nom = Filter::getString('nomMarmot');
        $url = Filter::getURL('urlMarmot');

        // Si $nom �s un String, entrar� a l'if. Altrament, $nom ser� fals.
        if($nom && $url) {
            $this->assign('allOk',true);
            $uploadModel = $this->getClass('GalleryGalleryModel');
            $uploadModel->setAnimal('marmot',$nom, $url);

        }
    }
}


?>