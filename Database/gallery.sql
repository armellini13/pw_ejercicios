-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-04-2014 a las 06:50:22
-- Versión del servidor: 5.6.16
-- Versión de PHP: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `gallery`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marmot`
--

CREATE TABLE IF NOT EXISTS `marmot` (
  `ID` varchar(10) NOT NULL DEFAULT '0',
  `Name` varchar(30) DEFAULT NULL,
  `URL` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marmot`
--

INSERT INTO `marmot` (`ID`, `Name`, `URL`) VALUES
('3', 'Mar 4', 'http://www.que.es/archivos/201302/dia_marmota_n-672xXx80.jpg'),
('6759marmot', 'marmot', 'http://www.carolglazerphotography.com/marmot_pika/bin/images/large/Yellow_Bellied_Marmot_Pups_1.jpg'),
('9087mar 4', 'mar 4', 'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTG-NvEJvsfXy9ItybRETy7_5Boox44byl8BY2fDxCSCpeLoMh1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `monkey`
--

CREATE TABLE IF NOT EXISTS `monkey` (
  `ID` varchar(10) NOT NULL DEFAULT '0',
  `Name` varchar(30) DEFAULT NULL,
  `URL` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `monkey`
--

INSERT INTO `monkey` (`ID`, `Name`, `URL`) VALUES
('11375Monke', 'Monkey 3', 'http://statfaking1.firstpost.in/wp-content/uploads/2012/06/monkey.jpg'),
('15724asdff', 'Monkeeeey', 'http://www.pageresource.com/wallpapers/wallpaper/animals-sweet-beautiful-high-definition-monkey-picture-free_313298.jpg'),
('25673Monke', 'Monkey 2', 'http://www.myredstar.com/wp-content/uploads/2013/09/lalala.jpg'),
('29520monke', 'monkey 4', 'http://impressivemagazine.com/wp-content/uploads/2013/07/interesting_facts_about_monkeys_cute_monkey.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `platypus`
--

CREATE TABLE IF NOT EXISTS `platypus` (
  `ID` varchar(10) NOT NULL DEFAULT '0',
  `Name` varchar(30) DEFAULT NULL,
  `URL` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `platypus`
--

INSERT INTO `platypus` (`ID`, `Name`, `URL`) VALUES
('0', 'plat5', 'http://images.nationalgeographic.com/wpf/media-live/photos/000/006/cache/platypus_662_600x450.jpg'),
('11059plat ', 'plat 66', 'http://www.australiantraveller.com/wp-content/uploads/2006/04/100-Things-To-Do-Before-You-Die-018-Spy-A-Platypus-In-The-Wild.jpg'),
('26972Plat ', 'Plat 3', 'http://gifts.worldwildlife.org/gift-center/Images/large-species-photo/large-Duck-billed-Platypus-photo.jpg'),
('3', 'Plat 4', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRGBZchPj_snXHUCzG2aN2zWcGGO0KjmPIklSKqmenPDO1rAfLo'),
('32599plat ', 'plat 55', 'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTMxyM5tWtPuZRHJvBepZZ9MpQ6ragmxcap5rQrwT7pYCORQLJthA');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
