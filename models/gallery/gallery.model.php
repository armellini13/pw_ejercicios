<?php

/*
 * Model que s'encarrega de guardar les fotos dels micos
 * que es pujan a la pàgina web.
 */
class GalleryGalleryModel extends Model
{

    // Genera un ID per la foto
    private function getMeAnimalID($table,$nom) {

        return rand().$nom;

    }


    // Guarda el l'animal dins de la DB
    public function setAnimal($table, $nom, $url) {

        $id = $this->getMeAnimalID($table,$nom);

        $sql = <<<QUERY
INSERT INTO
    $table
VALUES ( ?, ? , ? );
QUERY;


        $this->execute($sql,array( $id, $nom, $url));
    }

    public function changeAnimal($table, $id, $nom, $url) {

        $sql = <<<QUERY
UPDATE
    $table
SET
    Name='$nom',URL='$url'
WHERE
    ID='$id';
QUERY;

        $this->execute($sql);
    }

    // Genera un ID per la foto
    public function getMeAnimal($table) {

        $sql = <<<QUERY
SELECT
    ID, name, URL
FROM
    $table;
QUERY;

        return $this->getAll($sql);

    }

    // Genera un ID per la foto
    public function getMeAnimalByID($id,$table) {

        $sql = <<<QUERY
SELECT
    ID
FROM
    $table
WHERE
    ID = '$id';
QUERY;

        return $this->getAll($sql);

    }

    public function deleteAnimal($id,$table) {

        $sql = <<<QUERY
DELETE FROM
    $table
WHERE
    ID = '$id';
QUERY;

        $this->execute($sql);

    }

    public function getSize($table) {

        $sql = <<<QUERY
SELECT
    count(*)
FROM
    $table;
QUERY;

        return $this->getAll($sql)[0]['count(*)'];
    }

    public function getSizeGallery(){

        $platypus = $this->getSize('platypus');
        $monkey = $this->getSize('monkey');
        $marmot = $this->getSize('marmot');

        if($marmot >= $platypus && $marmot >= $monkey)
            return $marmot;
        else {
            if($platypus >= $monkey)
                return $platypus;
            else
                return $monkey;
        }
    }

}