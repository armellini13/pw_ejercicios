// Validation function
$(function() {
    // Setup form validation on the #myForm element
    $('#uploadPlatypus').validate({

        // Specify the validation rules
        rules: {

            nomPlatypus: {
                required: true
            },

            urlPlatypus: {
                required: true,
                url: true
            }
        },

        messages: {

            nomPlatypus: {
                required: "Has d'introduïr un nom per la imatge"
            },

            urlPlatypus: {
                required: "Has d'introduïr una URL per la imatge",
                url: "Has d'introduïr una URL vàlida per la imatge"
            }
        },

        submitHandler: function(form) {
            form.submit();
        }
    });
});

// Validation function
$(function() {
    // Setup form validation on the #myForm element
    $('#uploadMonkey').validate({

        // Specify the validation rules
        rules: {

            nomMonkey: {
                required: true
            },

            urlMonkey: {
                required: true,
                url: true
            }
        },

        messages: {

            nomMonkey: {
                required: "Has d'introduïr un nom per la imatge"
            },

            urlMonkey: {
                required: "Has d'introduïr una URL per la imatge",
                url: "Has d'introduïr una URL vàlida per la imatge"
            }
        },

        submitHandler: function(form) {
            form.submit();
        }
    });
});


// Validation function
$(function() {
    // Setup form validation on the #myForm element
    $('#uploadMarmot').validate({

        // Specify the validation rules
        rules: {

            nomMarmot: {
                required: true
            },

            urlMarmot: {
                required: true,
                url: true
            }
        },

        messages: {

            nomMarmot: {
                required: "Has d'introduïr un nom per la imatge"
            },

            urlMarmot: {
                required: "Has d'introduïr una URL per la imatge",
                url: "Has d'introduïr una URL vàlida per la imatge"
            }
        },

        submitHandler: function(form) {
            form.submit();
        }
    });
});

// Validation function
$(function() {
    // Setup form validation on the #myForm element
    $('#uploadImatge').validate({

        // Specify the validation rules
        rules: {

            nomImatge: {
                required: true
            },

            urlImatge: {
                required: true,
                url: true
            }
        },

        messages: {

            nomImatge: {
                required: "Has d'introduïr un nom per la imatge"
            },

            urlImatge: {
                required: "Has d'introduïr una URL per la imatge",
                url: "Has d'introduïr una URL vàlida per la imatge"
            }
        },

        submitHandler: function(form) {
            form.submit();
        }
    });
});


