<?php
/*
 * Archivo de configuraci�n de las clases que usaremos
 * Se llama desde Configure::getClass('NombreClase');
 */

/**
 * Engine:  Aquests par�metres no els toqueu.
 */
//$config['factory']						=  PATH_ENGINE . 'factory.class.php';
//$config['sql']							=  PATH_ENGINE . 'sql.class.php';


$config['mail']							=  PATH_ENGINE . 'mail.class.php';
$config['session']						=  PATH_ENGINE . 'session.class.php';
$config['user']							=  PATH_ENGINE . 'user.class.php';
$config['url']							=  PATH_ENGINE . 'url.class.php';
$config['uploader']						=  PATH_ENGINE . 'uploader.class.php';


$config['dispatcher']					=  PATH_ENGINE . 'dispatcher.class.php';


/** 
 * Controllers i Models 
 *
 * Aqui cal que cada nou controller i model que feu servir quedi declarat. 
 * Si no ho feu, no funcionar� qual el crideu des d'una ruta definida al fitxer dispatcher.config.php
 */

// Controller per 404
$config['ErrorError404Controller']		= PATH_CONTROLLERS . 'error/error404.ctrl.php';

// Controller Home i Model Home
$config['HomeHomeController']			= PATH_CONTROLLERS . 'home/home.ctrl.php';
$config['GalleryGalleryController']     = PATH_CONTROLLERS . 'gallery/gallery.ctrl.php';
$config['ModifyModifyController']       = PATH_CONTROLLERS . 'modify/modify.ctrl.php';
$config['UploadUploadController']       = PATH_CONTROLLERS . 'upload/upload.ctrl.php';
$config['HomeAboutController']          = PATH_CONTROLLERS . 'home/about.ctrl.php';

$config['GalleryGalleryModel']          = PATH_MODELS . 'gallery/gallery.model.php';

// Controllers compartits per tota la p�gina
$config['SharedHeadController']			= PATH_CONTROLLERS . 'shared/head.ctrl.php';
$config['SharedFooterController']		= PATH_CONTROLLERS . 'shared/footer.ctrl.php';

// Controllers de la galeria
$config['AnimalAnimalController']	    = PATH_CONTROLLERS . 'animal/animal.ctrl.php';
$config['AnimalMarmotController']		= PATH_CONTROLLERS . 'animal/marmot.ctrl.php';
$config['AnimalMonkeyController']		= PATH_CONTROLLERS . 'animal/monkey.ctrl.php';
$config['AnimalPlatypusController']	    = PATH_CONTROLLERS . 'animal/platypus.ctrl.php';

// Controllers del modify
$config['ModifyMarmotController']		= PATH_CONTROLLERS . 'modify/marmot.ctrl.php';
$config['ModifyMonkeyController']		= PATH_CONTROLLERS . 'modify/monkey.ctrl.php';
$config['ModifyPlatypusController']	    = PATH_CONTROLLERS . 'modify/platypus.ctrl.php';
$config['ModifyChangeController']		= PATH_CONTROLLERS . 'modify/change.ctrl.php';

// Controllers de l'upload
$config['UploadMarmotController']		= PATH_CONTROLLERS . 'upload/marmot.ctrl.php';
$config['UploadMonkeyController']		= PATH_CONTROLLERS . 'upload/monkey.ctrl.php';
$config['UploadPlatypusController']	    = PATH_CONTROLLERS . 'upload/platypus.ctrl.php';